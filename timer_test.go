package simpleTimer

import (
	"testing"

	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestTimer(t *testing.T) {
	sampleDuration := 102452.6234625634
	sampleTickrate := 0.5234764

	timer := NewTimer(sampleDuration)
	for i := float64(sampleTickrate); i < sampleDuration; i += sampleTickrate {
		timer.Tick(sampleTickrate)
	}
	tools.Test(!timer.Ended() && timer.Duration != 0, "timer has ended prematurely", t)
	timer.Tick(sampleTickrate)
	tools.Test(timer.Ended(), "timer has not ended on time", t)
}

func TestReset(t *testing.T) {
    timer := NewTimer(8)
    timer.Reset()
    tools.Test(timer.TimePassed == 0, "reset function did not reset counter", t)
}

func TestCarry(t *testing.T) {
    sampleDuration := 5.0
    sampleTimePassed := 8.0
    timer := NewTimer(sampleDuration)
    timer.TimePassed = sampleTimePassed
    timer.CarryReset()
    tools.Test(timer.TimePassed == sampleTimePassed - sampleDuration, "CarryReset did not properly carry", t)
}

func TestOffset(t *testing.T) {

    timer := NewTimer(5)
    timer.TimePassed = 3
    timer.LastUpdate = 2

    timer.Update(6)
    tools.Test(timer.TimePassed== 7, "did not set correct time with offset", t)

    timer.Tick(1)
    tools.Test(timer.TimePassed == 8, "did not tick correct time with offset", t)
}
