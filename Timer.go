package simpleTimer

type Timer struct {
	Duration    float64
    TimePassed float64
    LastUpdate float64
}

func NewTimer(duration float64) *Timer {
	timer := Timer{
		Duration:    duration,
        TimePassed: 0,
        LastUpdate: 0,
	}
	return &timer
}

func (t *Timer) Reset() {
    t.TimePassed = 0
}

func (t *Timer) CarryReset() {
	t.TimePassed -= t.Duration
}

func (t *Timer) Tick(timePassed float64) {
	t.TimePassed += timePassed
}

func (t *Timer) Update(currentTime float64) {
    t.TimePassed += currentTime - t.LastUpdate
    t.LastUpdate = currentTime
}

func (t *Timer) Ended() bool {
	//counting begins at 0 this is, why also when they are =
	return t.TimePassed >= t.Duration
}
